#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "variables.h"


int size_in_char(string filename){
    /*
    calculate the size of json file in char
        char == 1 byte
    */

    int res  = 0;  // output variable

    // open file and read
    FILE *file = fopen(filename, "r");
    if (!file ) {
        printf("ERROR OPEINING FILE\n");
        exit(cant_open_file);
    }

    // read char by char
    while (fgetc(file) != EOF ) {
        res++;
    }
    fclose(file);

    return res;
}

void store(string filename, char *data) {
    /*
    open file named by filename.
    read data and stored it in data[].
    data[] need to be created and memory allocated in main function
    */

    char buff = 1; // buffer for fgetc();
    int j = 0 ; // index of array data[];

    FILE *file = fopen(filename, "r");
    if (!file) {
        printf("ERROR OPENING FILE\n");
        printf("press enter to exit...");
        getc(stdin);
        exit(cant_open_file);
    }

    while ( buff  != EOF) {
        // read char by char
        buff = fgetc(file);

        // escape whitespace char during the process
        if ( buff != '\n' && buff != ' ' && buff != '\t') {
            data[j++] = buff;
        }
    }

    // add null terminated value at end of array
    data[j] = '\0';

    // close the file
    fclose(file);
}

void view(char *data) {
    /*
    print json object in console char by char
    */

    int j = 0; // index of array

    while (data[j] != '\0') {
        printf("%c", data[j]);
        j++;
    }
}

int obj_number(char *array) {
    /*
    calculate number of json object in json array,
    by calculating number of opend and closed bracket
    */

    // need to check this later, we didn't inlcude closed char
    int j = 0; // index of array
    int objNumber = 0; // return value
    int counter = 0; // bracket counter for opening and closing

    while ( array[j] != '\0') {

        if ( array[j] == '{') {
            counter++;
        }

        if (array[j] == '}') {
            counter--;
            if (counter == 0 ) {
                // when reach end of object
                objNumber++;
            }
        }

        j++;
    }

    return objNumber;
}
