#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// respect the order of include
#include "variables.h"

linked_list *init_list(string key, string value ){
    /*
    create and return new node with the key and value inserted
    */

    // allocate memory for new node
    linked_list *node = malloc(sizeof(node));
    // maximum size of key is 50 char
    node->key = malloc(sizeof(char) * 50 );
    // maximum size of value is 1000 char
    node->value = malloc(sizeof(char) * 1000);


    // insert key and value
    strcpy(node->key, key);
    strcpy(node->value, value);
    node->next = NULL;

    // return pointer the created node
    return node;
}

void add2list(linked_list *head, string key, string value ){
    /*
    add new key and value to existing linked_list,
    the linked_list need to be initialized using the funtion init_node
    */


    // set cursor to end of linked_lsit

    while (head->next != NULL ){
        head = head->next;
    }

    // create new node
    linked_list *new_node = init_list(key, value);

    // add new node
    head->next = new_node;
}

void disp_list(linked_list *head){
    /*
    display the linked list
    */

    if (head == NULL ){
        printf("list is empty\n");
    }

    while (head->next != NULL){
        printf("key : %s, value : %s\n\n", head->key, head->value);
        head = head->next;
    }

    // print the last element
    printf("key : %s, value : %s\n", head->key, head->value);
}

int linked_list_occurence(linked_list *head, string str){
    /* find the number of occurence of str in linked_list */
    /* go through linked list node by node */

    int occurence = 0;

    while ( head != NULL ) {
        if ( !strcmp(head->key, str)) {
            occurence++;
        }
        head = head->next;
    }
    return occurence;
}

linked_list* key_value2linked_list(char object[][10000], int number_of_object) {
    // retreive key value and store them in linked list

    linked_list *head = init_list("key", "value");
    int beta = 0; // index of object

    // variable to hold key and value
    char key[50];
    char value[1000];
    //string key = malloc(sizeof(char) * 25);
    //string value = malloc(sizeof(char) * 500);

    // definie some local variable
    int i  =0;
    int start = 0;
    int end =0;
    int k = 0;
    int x = 0;
    int array_counter = 0;
    int bracket_counter = 0;

    while (beta < number_of_object) {


        i = 0 ; // index of object
        start = 0;
        end = 0;


        while ( object[beta][i] != '\0' ) {
            //memset(key, '\0', 24);
            //memset(value, '\0', 499);
            //key[0] = '\0';
            //value[0] = '0';

            if ( object[beta][i] == '\"' ) {
                start = i + 1;
                k = i + 1;
                while ( object[beta][k] != '\"' ){
                    k++;
                }
                end = k -1;

                // copy key
                //strncpy(key, object[beta][start], end - start + 1);

                x = 0;
                for (int y = start ; y < end + 1  ; y++ ) {
                    key[x] = object[beta][y];
                    x++;
                }
                key[x] = '\0';

                //printf("%s\n", key);
                i = k + 2; // espace the semi colon, set the postion at value begining

                if ( object[beta][i] == '\"') {
                    // value begin with "
                    start = i+ 1;
                    k = i + 1;
                    while ( object[beta][k] != '\"') {
                        k++;
                    }
                    end = k -1;
                    //strncpy(value, object[beta][start], end - start + 1);

                    x = 0;
                    for (int y = start ; y < end + 1; y++ ) {
                        value[x++] = object[beta][y];
                    }
                    value[x] = '\0';

                    i = k;
                } else if ( object[beta][i] == '[') {
                    // value start with [ as array
                    start = i;
                    k = i;
                    while (( object[beta][k] != ']') || array_counter  ){
                        if ( object[beta][k] == ']') {
                            array_counter--;
                            if (array_counter == 0) {
                                break;
                            }
                        }
                        if (object[beta][k] == '[') {
                            array_counter++;
                        }
                        k++;
                    }
                    end = k;
                    //strncpy(value, object[beta][start] , end - start + 1);

                    x = 0;
                    for (int y = start ; y < end + 1; y++ ) {
                        value[x++] = object[beta][y];
                    }
                    value[x] = '\0';

                    i = k;
                } else if (object[beta][i] == '{') {

                    // value start with { as object
                    start = i;
                    k = i;
                    while (( object[beta][k] != '}') || bracket_counter  ){
                        if ( object[beta][k] == '}') {
                            bracket_counter--;
                            if (bracket_counter == 0) {
                                break;
                            }
                        }
                        if (object[beta][k] == '{') {
                            bracket_counter++;
                        }
                        k++;
                    }
                    end = k;
                    //strncpy(value, object[beta][start] , end - start + 1);

                    x = 0;
                    for (int y = start ; y < end + 1; y++ ) {
                        value[x++] = object[beta][y];
                    }
                    value[x] = '\0';

                    i = k;

                } else {
                    start = i;
                    do {
                        i++;

                    } while ( ( object[beta][i] != ',' ) && ( object[beta][i] != '}'));
                    end = i -1;
                    //strncpy(value, object[beta][start], end - start +1);

                    x = 0;
                    for (int y = start ; y < end + 1; y++ ) {
                        value[x++] = object[beta][y];
                    }
                    value[x] = '\0';

                }
                //printf("%s\n", value);
                // copy key value in linked list
                add2list(head, key, value);

            }
            i++;
        }
        beta++;
    }
    return head;
}
