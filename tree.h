#ifndef TREE_H_INCLUDED
#define TREE_H_INCLUDED

tree *init_tree(string key, string value);

void disp_leaf(tree *leaf);

tree* insert(tree *root, string key, string value);

void inorder_trav(tree *root);

int search_occurence(tree *root, string str);

tree* key_value2tree(char object[][10000], int number_of_object);

#endif // TREE_H_INCLUDED
