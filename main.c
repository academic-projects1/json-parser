#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// respect the order of include library
#include "variables.h"
#include "json_mapper.h"
#include "linked_list.h"
#include "tree.h"


int main () {

    // define local variables
    int CHAR_SIZE = size_in_char("main.json"); // size of file in bytes, each char represents a byte
    int NUMBER_OF_OBJECT = 0; // number of object in json file


    // array holding json data char by char
    char *data = malloc( sizeof(char)* CHAR_SIZE );
    store("main.json", data);


    // copy data from data array created above, organize them, then store them in two dim array
    NUMBER_OF_OBJECT = obj_number(data);
    // set maximum size of an object to 10 000 bytes
    char json_data[NUMBER_OF_OBJECT][10000]; // this is the array which we'll be working with

    // replace garabage values by \0
    for (int i = 0; i < NUMBER_OF_OBJECT; i++){
        memset(json_data[i], '\0', 10000);
    }

    /////////////////////////////////////////////////////////////////////////////
    // WARNING : DON'T TOUCH THIS CODE
    // copy from data[] into json_data[][]
    ////////////////////////////////////////////////////////////////////////////
    int j = 0; // index of data array
    int bracket_counter = 0; // conter for  opning and closing of json object
    int obj_index = 0; // index of json_data array

    while( data[j] != '\0') {

        // start & end of an object
        // k temporay index for each object
        int k = 0, start = 0 , end = 0;

        if ( data[j] == '{') {

            if (bracket_counter == 0 ) {
                // begining of json object
                start = j; // stroe the value of begining
                bracket_counter++;
                k = j; // init the temp index

                do {
                    // keep going until the end of json object
                    k++;
                    if (data[k] == '{') {
                        bracket_counter++;
                    }
                    if ( data[k] == '}') {
                        bracket_counter--;
                    }
                } while (/*data[k] != '}' &&*/ bracket_counter != 0);
                end = k; // store end of json object

                // copy copy json object into json_data
                strncpy(json_data + obj_index, data + start , end - start + 2 );
                json_data[obj_index][end - start + 1] = '\0'; // add null terminated value at end
                obj_index++;

            }

            j = k; // move data index to end of current object and contineu the parsing
        }
        j++;
    }
    /////////////////////////////////////////////////////////////////////////////
    // WARNING DON'T TOUCH THE CODE ABOVE
    ////////////////////////////////////////////////////////////////////////////

    /*
    for (int i = 0; i < NUMBER_OF_OBJECT; i++ ) {
        printf("strlen : %d %s\n\n", strlen(json_data[i]), json_data[i]);
    }
    */
    printf("the json file contains %d objects\n", NUMBER_OF_OBJECT);

    /* ************************* */
    /* IMPLEMENTATION WITH TREES */
    /* ************************* */
/*
    // create tree and copy data
    tree *root = key_value2tree(json_data, NUMBER_OF_OBJECT);
    // inorder traversal of tree
    inorder_trav(root);

    // calc execution time
    int occurence = 0;
    clock_t start = clock();

    for ( int i = 0; i< 1000; i++ ) {
        // time is so small, so loop 1000 time to get a value, then devide
        occurence = search_occurence(root, "username");
    }

    clock_t end = clock();
    puts("\n-----------------------------------------");
    printf("the key username appeared %d times\n", search_occurence(root, "username"));
    printf("time of execution : %.10f sec \n", (double)(end - start) / CLOCKS_PER_SEC);
    puts("-----------------------------------------");
*/

    /* ******************************* */
    /* IMPLEMENTATION WITH LINKED LIST */
    /* ******************************* */

    // create linked list node and copy data
    linked_list *head = key_value2linked_list(json_data, NUMBER_OF_OBJECT);
    disp_list(head);

    // calc execution time
    int occurence = 0;
    clock_t start = clock();

    for (int i = 0 ; i < 1000; i++) {
        // time is so small, so loop 1000 times to get a value then devide
        occurence = linked_list_occurence(head, "username");
    }

    clock_t end = clock();

    puts("\n-----------------------------------------");
    printf("the key username appeard %d times\n", occurence);
    printf("time of execution : %.10f sec \n", (double)(end - start) / CLOCKS_PER_SEC);
    puts("------------------------------------------");


    // stop the console from closing
    printf("press enter to exit ...\n");
    getc(stdin);
    return 0;
}
