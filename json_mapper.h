#ifndef JSON_MAPPER_H_INCLUDED
#define JSON_MAPPER_H_INCLUDED

/*
define function used to read data from file
*/


int size_in_char(string filename);

void store(string filename, char *data);

void view(char *data);

int obj_number(char *array);

#endif // JSON_MAPPER_H_INCLUDED
