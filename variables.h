#ifndef VARIABLES_H_INCLUDED
#define VARIABLES_H_INCLUDED


typedef char* string;

enum exit_code {
    cant_open_file,
    cant_allocate_mem
};

typedef struct linked_list {
    string key;
    string value;

    struct linked_list *next;
} linked_list;

typedef struct tree {
    string key;
    string value;

    // right and left child
    struct tree *rchild;
    struct tree *lchild;
} tree ;

#endif // VARIABLES_H_INCLUDED
