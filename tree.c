#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "variables.h"

tree *init_tree(string key, string value) {
    /* create new node with key, value and return a pointer */

    // allocate memeory
    tree *root = malloc(sizeof(tree));
    root->key = malloc(sizeof(char) * 50);
    root->value = malloc(sizeof(char) * 1000);

    // add key and value
    strcpy(root->key, key);
    strcpy(root->value, value);
    root->rchild = NULL;
    root->lchild = NULL;

    return root;
}

void disp_leaf(tree *leaf) {
    /* display information about single node in tree */

    printf("key : %s\n", leaf->key);
    printf("value : %s\n", leaf->value);
    printf("pointer : %p\n", leaf);
    printf("right child : %p\n", leaf->rchild);
    printf("left child : %p\n\n", leaf->lchild);
}

tree* insert(tree *root, string key, string value) {
    /* insert key, value in binary search tree */
    /* the comparison between tree is based on ascci value comparison
    with the built-in function strcmp()
    */

    if (root == NULL) {
        // if root is null create new on
        root = init_tree(key, value);
    } else if ( strcmp(root->key, key) > 0) {
        // if key is lesser, go left
        root->lchild = insert(root->lchild, key, value);
    } else if (strcmp(root->key, key) < 0 ){
        // if key is greater go right
        root->rchild = insert(root->rchild, key, value);
    } else {
        // case of equality of values: compare values instead
        if ( strcmp(root->value, value) > 0 ) {
            // if value is lesser, go left
            root->lchild = insert(root->lchild, key, value);
        }
        else {
            // if value is greater go right
            root->rchild = insert(root->rchild, key, value);
        }

    }

    return root;
}

void inorder_trav(tree *root) {
    /* inorder traversal function */

    if (root == NULL) {
        return;
    }

    inorder_trav(root->lchild);
    printf("key : %s, value : %s\n", root->key, root->value);
    inorder_trav(root->rchild);
}

int search_occurence(tree *root, string str) {
    if (root == NULL ){
        return 0;
    } else if (strcmp(root->key, str) > 0) {
        search_occurence(root->lchild, str);
    } else if (strcmp(root->key, str ) < 0 ){
        search_occurence(root->rchild, str);
    } else {
        // case of equality
        return 1 + search_occurence(root->lchild, str) + search_occurence(root->rchild, str);
    }
}

tree* key_value2tree(char object[][10000], int number_of_object) {
    // retreive key value and store them in linked list

    tree *root = NULL;
    int beta = 0; // index of object

    char key[50];
    char value[1000];
    //string key = malloc(sizeof(char) * 25);
    //string value = malloc(sizeof(char) * 500);

    int i  =0;
    int start = 0;
    int end =0;
    int k = 0;
    int x = 0;
    int array_counter = 0;
    int bracket_counter = 0;

    while (beta < number_of_object) {


        i = 0 ; // index of object
        start = 0;
        end = 0;


        while ( object[beta][i] != '\0' ) {
            //memset(key, '\0', 24);
            //memset(value, '\0', 499);
            key[0] = '\0';
            value[0] = '0';
            if ( object[beta][i] == '\"' ) {
                start = i + 1;
                k = i + 1;
                while ( object[beta][k] != '\"' ){
                    k++;
                }
                end = k -1;

                // copy key
                //strncpy(key, object[beta][start], end - start + 1);

                x = 0;
                for (int y = start ; y < end + 1  ; y++ ) {
                    key[x] = object[beta][y];
                    x++;
                }
                key[x] = '\0';

                //printf("%s\n", key);
                i = k + 2; // espace the semi colon, set the postion at value begining

                if ( object[beta][i] == '\"') {
                    // value begin with "
                    start = i+ 1;
                    k = i + 1;
                    while ( object[beta][k] != '\"') {
                        k++;
                    }
                    end = k -1;
                    //strncpy(value, object[beta][start], end - start + 1);

                    x = 0;
                    for (int y = start ; y < end + 1; y++ ) {
                        value[x++] = object[beta][y];
                    }
                    value[x] = '\0';

                    i = k;
                } else if ( object[beta][i] == '[') {
                    // value start with [ as array
                    start = i;
                    k = i;
                    while (( object[beta][k] != ']') || array_counter  ){
                        if ( object[beta][k] == ']') {
                            array_counter--;
                            if (array_counter == 0) {
                                break;
                            }
                        }
                        if (object[beta][k] == '[') {
                            array_counter++;
                        }
                        k++;
                    }
                    end = k;
                    //strncpy(value, object[beta][start] , end - start + 1);

                    x = 0;
                    for (int y = start ; y < end + 1; y++ ) {
                        value[x++] = object[beta][y];
                    }
                    value[x] = '\0';

                    i = k;
                } else if (object[beta][i] == '{') {

                    // value start with { as object
                    start = i;
                    k = i;
                    while (( object[beta][k] != '}') || bracket_counter  ){
                        if ( object[beta][k] == '}') {
                            bracket_counter--;
                            if (bracket_counter == 0) {
                                break;
                            }
                        }
                        if (object[beta][k] == '{') {
                            bracket_counter++;
                        }
                        k++;
                    }
                    end = k;
                    //strncpy(value, object[beta][start] , end - start + 1);

                    x = 0;
                    for (int y = start ; y < end + 1; y++ ) {
                        value[x++] = object[beta][y];
                    }
                    value[x] = '\0';

                    i = k;



                } else {
                    start = i;
                    do {
                        i++;

                    } while ( ( object[beta][i] != ',' ) && ( object[beta][i] != '}'));
                    end = i -1;
                    //strncpy(value, object[beta][start], end - start +1);

                    x = 0;
                    for (int y = start ; y < end + 1; y++ ) {
                        value[x++] = object[beta][y];
                    }
                    value[x] = '\0';

                }
                //printf("%s\n", value);
                // copy key value in linked list
                root = insert(root, key, value);

            }
            i++;
        }
        beta++;
    }
    return root;
}
