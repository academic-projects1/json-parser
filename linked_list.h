#ifndef LINKED_LIST_H_INCLUDED
#define LINKED_LIST_H_INCLUDED

linked_list *init_list(string key, string value);

void add2list(linked_list *head, string key, string value );

void disp_list(linked_list *head);

int linked_list_occurence(linked_list *head, string str);

linked_list* key_value2linked_list(char object[][10000], int number_of_object);

#endif // LINKED_LIST_H_INCLUDED
